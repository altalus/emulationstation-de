# Modern-DE Theme for Emulationstation Desktop Edition

The modern theme is a new theme for ES-DE that supports most latest features such as badges and controllers. The theme
is meant to offer a modern look for ES-DE. The theme is designed with theme variants in mind (v1.3).

We support 8 variants:

- 4:3 dark
- 4:3 light
- 16:9 dark
- 16:9 light
- 16:10 dark
- 16:10 light
- 21:9 dark
- 21:9 light

The variant that is to be used can be selected by changing line 14 of `theme.xml`. This will be made accessible through
the user interface in future updates, specifically v1.3:

```{xml}
<include>./variants/dark21_9/default.xml</include>
```

# Based on

The theme is based on '[es-theme-switch](https://github.com/lilbud/es-theme-switch)' by lilbud.

# Changes

The following changes to the theme are made:

- support for 4:3, 16:9, 21:9 resolutions
- graphics for all ES-DE supported systems
- placeholder styling for unknown systems
- badges icons (broken, favorite, etc...)
- controller icons
- new font
- restyled rightmost metadata panel
- new rating icons

# Pictures

![](https://gitlab.com/leonstyhre/emulationstation-de/uploads/8859cd1f6ebf5653b6eb12b4f5171ecc/image.png)
![](https://gitlab.com/leonstyhre/emulationstation-de/uploads/920a19cf845cb65f98db4cee5158a26f/image.png)
![](https://gitlab.com/leonstyhre/emulationstation-de/uploads/f3d7ac00964e92252bea000043c1cab5/image.png)
![](https://gitlab.com/leonstyhre/emulationstation-de/uploads/7f317ad1dff31e0c2dd21e4d1930613d/image.png)
![](https://gitlab.com/leonstyhre/emulationstation-de/uploads/6a87a0d94898cfe060b028e29f148147/image.png)
![](https://gitlab.com/leonstyhre/emulationstation-de/uploads/6862b35b9142888f983d86fea65d5411/image.png)
![](https://gitlab.com/leonstyhre/emulationstation-de/uploads/36994f94784ff12c0ea48520544ed4fb/image.png)