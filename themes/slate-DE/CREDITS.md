# EmulationStation Desktop Edition (ES-DE) - slate-DE credits

The theme set slate-DE is based on recalbox-multi by the Recalbox community.

Some graphics was also taken from the carbon theme by Rookervik.

## Original vector graphics for slate-DE

- Matthew Stapleton
- Leon Styhre

## recalbox-multi

- Supernature2k
- Paradadf
- Zuco
- Sm3ck
- Th4Shin
- Shub
- Lester
- MarbleMad
- Odissine
- Zarroun
- Reivaax
- RockAddicted
- Bigboo3000
- Paradadf

## carbon

- Rookervik
- Nils Bonenberger (original simple theme on which carbon is based)
