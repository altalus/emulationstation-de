//  SPDX-License-Identifier: MIT
//
//  EmulationStation Desktop Edition
//  GridGamelistView.h
//
//  Interface that defines a GamelistView of the type 'grid'.
//

#ifndef ES_APP_VIEWS_GAMELIST_GRID_GAMELIST_VIEW_H
#define ES_APP_VIEWS_GAMELIST_GRID_GAMELIST_VIEW_H

#include "components/BadgeComponent.h"
#include "components/DateTimeComponent.h"
#include "components/ImageGridComponent.h"
#include "components/RatingComponent.h"
#include "components/ScrollableContainer.h"
#include "components/VideoComponent.h"
#include "views/gamelist/ISimpleGamelistView.h"

class GridGamelistView : public ISimpleGamelistView
{
public:
    GridGamelistView(Window* window, FileData* root);
    virtual ~GridGamelistView() {}

    // Called when a FileData* is added, has its metadata changed, or is removed.
    void onFileChanged(FileData* file, bool reloadGamelist) override;

    void onShow() override;
    void onThemeChanged(const std::shared_ptr<ThemeData>& theme) override;
    void setCursor(FileData* cursor) override;

    FileData* getCursor() override { return mGrid.getSelected(); }
    FileData* getNextEntry() override { return mGrid.getNext(); }
    FileData* getPreviousEntry() override { return mGrid.getPrevious(); }
    FileData* getFirstEntry() override { return mGrid.getFirst(); }
    FileData* getLastEntry() override { return mGrid.getLast(); }
    FileData* getFirstGameEntry() override { return firstGameEntry; }

    std::string getName() const override { return "grid"; }

    bool input(InputConfig* config, Input input) override;

    std::vector<HelpPrompt> getHelpPrompts() override;
    void launch(FileData* game) override;

    bool isListScrolling() override { return mGrid.isScrolling(); }
    void stopListScrolling() override
    {
        mGrid.stopAllAnimations();
        mGrid.stopScrolling();
    }

    const std::vector<std::string>& getFirstLetterIndex() override { return mFirstLetterIndex; }

    void addPlaceholder(FileData* firstEntry = nullptr) override;

protected:
    std::string getQuickSystemSelectRightButton() override { return "rightshoulder"; }
    std::string getQuickSystemSelectLeftButton() override { return "leftshoulder"; }
    void populateList(const std::vector<FileData*>& files, FileData* firstEntry) override;
    void remove(FileData* game, bool deleteFile) override;
    void removeMedia(FileData* game) override;
    void update(int deltaTime) override;

    ImageGridComponent<FileData*> mGrid;
    // Points to the first game in the list, i.e. the first entry which is of the type 'GAME'.
    FileData* firstGameEntry;

private:
    void updateInfoPanel();
    const std::string getImagePath(FileData* file);

    void initMDLabels();
    void initMDValues();

    ImageComponent mMarquee;
    ImageComponent mImage;

    TextComponent mLblRating;
    TextComponent mLblReleaseDate;
    TextComponent mLblDeveloper;
    TextComponent mLblPublisher;
    TextComponent mLblGenre;
    TextComponent mLblPlayers;
    TextComponent mLblLastPlayed;
    TextComponent mLblPlayCount;

    BadgeComponent mBadges;
    RatingComponent mRating;
    DateTimeComponent mReleaseDate;
    TextComponent mDeveloper;
    TextComponent mPublisher;
    TextComponent mGenre;
    TextComponent mPlayers;
    DateTimeComponent mLastPlayed;
    TextComponent mPlayCount;
    TextComponent mName;

    std::vector<TextComponent*> getMDLabels();
    std::vector<GuiComponent*> getMDValues();

    ScrollableContainer mDescContainer;
    TextComponent mDescription;
    TextComponent mGamelistInfo;
};

#endif // ES_APP_VIEWS_GAMELIST_GRID_GAMELIST_VIEW_H
