EmulationStation Desktop Edition (ES-DE) - Portable installation on Windows
---------------------------------------------------------------------------

ES-DE release:
1.2.3

Instructions:

1) The ROMs_ALL directory contains all the systems that ES-DE supports, but to decrease application startup time, only copy the systems you need to the ROMs directory.
2) Copy your games into the respective folders in the ROMs directory.
3) Place your emulators inside the Emulators directory.
4) Optionally copy your existing gamelists, scraped/downloaded media, custom collections, custom systems and themes to the .emulationstation directory.
5) Start ES-DE using EmulationStation.exe and enjoy some retrogaming!

In case of issues, check .emulationstation\es_log.txt for clues as to what went wrong.
Starting EmulationStation.exe with the --debug flag will provide even more details.

This portable release contains a specific es_find_rules.xml file that will only look for emulators inside the portable directory tree.
If you would like to use the default configuration file instead which will also look for emulators elsewhere on your system then go to resources\systems\windows\
and delete the es_find_rules.xml file and rename es_find_rules_default.xml to es_find_rules.xml

Preconfigured emulator locations:

Emulators\RetroArch-Win64\retroarch.exe
Emulators\RetroArch\retroarch.exe
Emulators\atari800\atari800.exe
Emulators\cemu\Cemu.exe
Emulators\Citra\canary-mingw\citra.exe
Emulators\Citra\nightly-mingw\citra.exe
Emulators\Dolphin-x64\Dolphin.exe
Emulators\dosbox-staging\dosbox.exe
Emulators\duckstation\duckstation-nogui-x64-ReleaseLTCG.exe
Emulators\duckstation\duckstation-qt-x64-ReleaseLTCG.exe
Emulators\flycast\flycast.exe
Emulators\mame\mame.exe
Emulators\melonDS\melonDS.exe
Emulators\mGBA\mGBA.exe
Emulators\mupen64plus\mupen64plus-ui-console.exe
Emulators\PCSX2\pcsx2.exe
Emulators\Play\Play.exe
Emulators\PPSSPP\PPSSPPWindows64.exe
Emulators\redream\redream.exe
Emulators\RPCS3\rpcs3.exe
Emulators\ryujinx\Ryujinx.exe
Emulators\snes9x\snes9x-x64.exe
Emulators\VBA-M\visualboyadvance-m.exe
Emulators\xemu\xemu.exe
Emulators\xenia\xenia.exe
Emulators\yuzu\yuzu-windows-msvc\yuzu.exe
